#define NULL 1

#define MAX_LENGTH 15

void UIntToHexStr(unsigned int uiValue, char pcStr[]){
		
	unsigned char ucNybbleCounter;
	unsigned char ucNybble;
	
	pcStr[0]='0';
	pcStr[1]='x';
	
	for(ucNybbleCounter=0; ucNybbleCounter<4; ucNybbleCounter++){
		
		ucNybble=(uiValue>>(ucNybbleCounter*4))&0xF;
		
		if(ucNybble<10){
			pcStr[5-ucNybbleCounter]='0'+ucNybble;
		}
		else{
			pcStr[5-ucNybbleCounter]='A'+(ucNybble-10);
		}
	}
}

enum Result {OK, ERROR};

enum Result eHexStringToUInt(char pcStr[], unsigned int *puiValue){
	unsigned char ucCharacterCounter;
	unsigned char ucNybble;
	
	*puiValue=0;
	
	if ((pcStr[0]!='0')||( pcStr[1]!='x')||( pcStr[2]==NULL)){
return ERROR;
}

	for (ucCharacterCounter=2; pcStr[ucCharacterCounter]!=NULL; ucCharacterCounter++){
		ucNybble=pcStr[ucCharacterCounter];
		
		if(ucCharacterCounter==6){
			return ERROR;
		}
		
		*puiValue = *puiValue << 4;
		
		if ((ucNybble <= '9') && (ucNybble >= '0')){
			*puiValue = *puiValue | (ucNybble - '0');
		}
		else if ((ucNybble <= 'F') && (ucNybble >= 'A')){
			*puiValue= *puiValue | (ucNybble -'A'+10);
		}
		else{
			return ERROR;
		}
	}
	
	return OK;
}

void AppendUIntToString(unsigned int uiValue, char pcDestinationStr[]){
	unsigned char ucStringLength;
	
	for (ucStringLength=0; pcDestinationStr[ucStringLength]!=NULL; ucStringLength++){}
		
	UIntToHexStr(uiValue, &pcDestinationStr[ucStringLength]);
}


int main(){
	
	char acStringA[254] = "testy!";
	//char acStringC[254];
	
	enum Result eRes = ERROR;
	
	unsigned int uiValue;
	char acStringC[] = "0x000000000";
	//char acStringC[] = "0x123456789";
	UIntToHexStr(65000, acStringC);
	
	
	
	//eRes = eHexStringToUInt(acStringC, &uiValue);
	
	//AppendUIntToString(65000, acStringA);
	
	/*char pcStr[MAX_LENGTH];
	unsigned int *puiValue;
	char pcExample1[MAX_LENGTH] = "";
	char pcExample2[MAX_LENGTH] = "0x";
	int eRes;
	unsigned int uiValue = 1245;
	unsigned char ucDestinationStr[MAX_LENGTH];
	
	UIntToHexStr (uiValue, pcStr);
	
	eRes = eHexStringToUInt(pcExample1, puiValue);
	eRes = eHexStringToUInt(pcExample2, puiValue);
	
	AppendUIntToString(65000, ucDestinationStr);*/
}
